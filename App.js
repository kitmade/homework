import React from "react";
import { LogBox, StyleSheet, Text, View } from "react-native";
import RootNavigation from "./src/navigation";
export default class App extends React.Component {
  render() {
    return <RootNavigation />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
