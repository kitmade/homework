import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const ContactDetialField = ({ label, data = [], onItemPress = () => {} }) => (
  <View style={styles.container}>
    <Text style={styles.label}>{label}</Text>
    {data.map((item) => (
      <TouchableOpacity
        key={item}
        style={styles.fieldItem}
        onPress={() => onItemPress(item)}
      >
        <Text>{item}</Text>
      </TouchableOpacity>
    ))}
  </View>
);

export default ContactDetialField;

const styles = StyleSheet.create({
  container: {
    marginBottom: 12,
  },
  label: {
    fontWeight: "bold",
    fontSize: 22,
  },
  fieldItem: {
    backgroundColor: "white",
    paddingVertical: 16,
    paddingHorizontal: 8,
    marginTop: 12,
  },
});
