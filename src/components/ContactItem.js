import React, { useState } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import InAppImage from "./InAppImage";

const ContactItem = ({
  contactDetail: { fullname, phoneNumbers, emails, urlAddresses, isFavorited },
  onPress = () => {},
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text numberOfLines={1}>{fullname.trim() || phoneNumbers[0]}</Text>
      <InAppImage
        imageName={isFavorited ? "star-favorited" : "star-empty"}
        style={styles.favorite}
      />
    </TouchableOpacity>
  );
};

export default ContactItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: "white",
    borderTopWidth: 1,
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  favorite: {
    height: 30,
    width: 30,
  },
});
