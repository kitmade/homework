import React, { useMemo } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

const InAppImage = ({ imageName, style, onPress = () => {} }) => {
  const imagePath = useMemo(() => {
    switch (imageName) {
      case "star-empty":
        return require("../../assets/icons/star-empty.png");
      case "star-favorited":
        return require("../../assets/icons/star-favorited.png");
    }
  }, [imageName]);

  return (
    <TouchableOpacity onPress={onPress}>
      <Image source={imagePath} style={style} />
    </TouchableOpacity>
  );
};

export default InAppImage;
