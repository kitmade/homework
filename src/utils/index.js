const formatContactList = (contactList) =>
  contactList.map(
    ({ id, firstName, lastName, emails, phoneNumbers, urlAddresses }) => {
      return {
        id: id,
        fullname: `${firstName || ""} ${lastName || ""}`,
        emails: emails?.map(({ email }) => email),
        phoneNumbers: phoneNumbers?.map(({ number }) => number),
        urlAddresses: urlAddresses?.map(({ url }) => url),
        isFavorited: false,
      };
    }
  );

const sortedContactWithFavorite = (contactList, favoritedId) =>
  contactList
    .map((contact) => {
      if (contact.id === favoritedId) {
        return { ...contact, isFavorited: true };
      } else {
        return { ...contact, isFavorited: false };
      }
    })
    .sort((contact) => contact.isFavorited !== true);

export { formatContactList, sortedContactWithFavorite };
