import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation, useRoute } from "@react-navigation/native";
import React, { useCallback, useEffect, useState } from "react";
import { FlatList, ScrollView, StyleSheet, Text, View } from "react-native";
import { phonecall, email, web } from "react-native-communications";
import ContactDetialField from "../components/ContactDetailField";
import InAppImage from "../components/InAppImage";
const ContactDetail = ({ params }) => {
  const navigation = useNavigation();
  const route = useRoute();
  const { contactDetail } = route.params;
  const { id, fullname, emails, phoneNumbers, urlAddresses, isFavorited } =
    contactDetail;
  const [isMarked, setMarked] = useState(isFavorited);

  const onFavoritePress = useCallback(
    () =>
      setMarked((prev) => {
        if (!prev) {
          AsyncStorage.setItem("favoritedContactId", JSON.stringify(id));
        } else {
          AsyncStorage.removeItem("favoritedContactId");
        }
        return !prev;
      }),
    [isMarked]
  );

  const hanldePhoneNumberPress = (phoneNumber) => {
    phonecall(phoneNumber, false);
  };

  const handleEmailPress = (emailAddress) => {
    email(emailAddress, null, null, null, null);
  };
  const handleUrlPress = (url) => {
    web(url);
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.contactName}>{fullname}</Text>
      <InAppImage
        imageName={isMarked ? "star-favorited" : "star-empty"}
        style={styles.favorite}
        onPress={onFavoritePress}
      />
      <ContactDetialField
        label="Phone numbers"
        data={phoneNumbers}
        onItemPress={hanldePhoneNumberPress}
      />
      <ContactDetialField
        label="Emails"
        data={emails}
        onItemPress={handleEmailPress}
      />
      <ContactDetialField
        label="URLs"
        data={urlAddresses}
        onItemPress={handleUrlPress}
      />
    </ScrollView>
  );
};

export default ContactDetail;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  contactName: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 24,
    marginTop: 22,
    marginBottom: 8,
  },
  favorite: {
    height: 45,
    width: 45,
    alignSelf: "center",
    marginBottom: 22,
  },
});
