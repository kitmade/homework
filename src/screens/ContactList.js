import React, { useEffect, useMemo, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import * as Contacts from "expo-contacts";
import { useIsFocused, useNavigation } from "@react-navigation/native";
import SCREENS from "../navigation/screenName";
import ContactDetail from "./ContactDetail";
import ContactItem from "../components/ContactItem";
import { formatContactList, sortedContactWithFavorite } from "../utils";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getContactsAsync } from "../services/contact";

const ContactList = ({ params }) => {
  const navigation = useNavigation();
  const [searchText, setSearchText] = useState("");
  const [contactList, setContactList] = useState([]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      AsyncStorage.getItem("favoritedContactId").then((res) => {
        const favoritedContactId = JSON.parse(res);
        if (!!favoritedContactId) {
          setContactList((prev) =>
            sortedContactWithFavorite(prev, favoritedContactId)
          );
        }
      });
    });
    return unsubscribe;
  }, []);

  useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();

      if (status === "granted") {
        const { data } = await getContactsAsync(searchText);
        if (data.length > 0) {
          const favoritedContactId = await AsyncStorage.getItem(
            "favoritedContactId"
          ).then((res) => JSON.parse(res));
          const filteredContactList = !!favoritedContactId
            ? sortedContactWithFavorite(
                formatContactList(data),
                favoritedContactId
              )
            : formatContactList(data);
          setContactList(filteredContactList);
        }
      }
    })();
  }, [searchText]);

  const renderSearchBar = useMemo(
    () => () =>
      (
        <TextInput
          placeholder="Enter name or phone number here"
          style={styles.searchBar}
          onChangeText={setSearchText}
        />
      ),
    []
  );

  const onContactPress = (contactDetail, index) => {
    navigation.navigate(SCREENS.CONTACT_DETAIL, {
      contactDetail,
    });
  };

  return (
    <FlatList
      ListHeaderComponent={renderSearchBar}
      showsVerticalScrollIndicator={false}
      stickyHeaderIndices={[0]}
      data={contactList}
      maxToRenderPerBatch={15}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({ item, index }) => (
        <ContactItem
          onPress={() => onContactPress(item, index)}
          contactDetail={item}
        />
      )}
    />
  );
};

export default ContactList;

const styles = StyleSheet.create({
  searchBar: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    backgroundColor: "gray",
  },
});
