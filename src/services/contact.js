import * as Contacts from "expo-contacts";

export const getContactsAsync = async (searchText) =>
  await Contacts.getContactsAsync({
    fields: [
      Contacts.Fields.Emails,
      Contacts.Fields.PhoneNumbers,
      Contacts.Fields.UrlAddresses,
    ],
    pageSize: 500,
    name: searchText,
    sort: Contacts.SortTypes.FirstName,
  });
