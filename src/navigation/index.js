import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import { Text, View } from "react-native";
import ContactRoutes from "./contactRouter";
import SCREENS from "./screenName";

const RootNavigator = createNativeStackNavigator();

export default ({ params }) => (
  <NavigationContainer>
    <RootNavigator.Navigator initialRouteName={SCREENS.CONTACT_LIST}>
      {[...ContactRoutes].map((screen) => {
        return (
          <RootNavigator.Screen
            key={screen.name}
            {...screen}
            options={{ headerTitleAlign: "center", ...screen.options }}
          />
        );
      })}
    </RootNavigator.Navigator>
  </NavigationContainer>
);
