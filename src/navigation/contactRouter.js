import ContactDetail from "../screens/ContactDetail";
import ContactList from "../screens/ContactList";
import SCREENS from "./screenName";

const ContactRoutes = [
  {
    name: SCREENS.CONTACT_DETAIL,
    component: ContactDetail,
    options: { headerTitle: "Contact detail" },
  },
  {
    name: SCREENS.CONTACT_LIST,
    component: ContactList,
    options: { headerTitle: "Contact list" },
  },
];

export default ContactRoutes;
