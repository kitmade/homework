# React Native Interview Homework
The goal of the developer exercise is to take a quick dive into a React Native + Expo project and build a two screen contact manager app. Left Field Labs will be evaluating the code and will schedule a walkthrough call with the developer candidate to discuss design choices.

## Expo published link:
https://expo.dev/@kitmade/contact-manager?serviceType=classic&distribution=expo-go

## Install instruction:
- <code>npm install</code> to install dependencies
- <code>npx expo-cli r -c</code> to start the Metro
- After that, you can scan the generated QR code on real device or follow the instruction to install on simulator devices.
